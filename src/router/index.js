import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Menu from "../views/Menu.vue";
import Section from "../views/Section.vue";
import Pay from "../views/Pay.vue";
import AdminLogin from "../views/AdminLogin.vue";
import Orders from "../views/Orders.vue";
import SingleOrder from "../views/SingleOrder.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/Menu",
    name: "Menu",
    component: Menu,
  },
  {
    path: "/Section/:section",
    name: "Section",
    component: Section,
  },
  {
    path: "/Pay",
    name: "Pay",
    component: Pay,
  },
  {
    path: "/AdminLogin",
    name: "AdminLogin",
    component: AdminLogin,
  },
  {
    path: "/Orders",
    name: "Orders",
    component: Orders,
  },
  {
    path: "/SingleOrder/:singleOrder",
    name: "SingleOrder",
    component: SingleOrder,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
